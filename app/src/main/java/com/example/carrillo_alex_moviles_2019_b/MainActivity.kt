package com.example.carrillo_alex_moviles_2019_b

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var gamescoreTextView : TextView
    private lateinit var tapMeButton : Button
    private lateinit var startButton : Button

    private lateinit var countDownTimer : CountDownTimer
    private var countDownInterval : Long = 1000

    private var timeLeft = 10
    private var gameScore = 0
    private var isGameStarted = false
    private var randomNumber = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        gamescoreTextView = findViewById(R.id.text_points)
        startButton = findViewById(R.id.button_inicio)
        tapMeButton= findViewById(R.id.button_points)


        startButton.setOnClickListener {
            randomNumber = (0..10).shuffled().first()
            startGame()
        }


    }

    private fun startGame(){
        configCountDownTimer()
        countDownTimer.start()

    }

    private fun endGame(){
        //Toast.makeText(this, getString(R.string.game_over, gameScore), Toast.LENGTH_LONG).show()
        resetGame()
    }
    
    private fun configCountDownTimer(){
        countDownTimer = object : CountDownTimer((timeLeft*1000).toLong(), countDownInterval){

            override fun onFinish() {
                endGame()
            }
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000

                tapMeButton.setOnClickListener {
                    if(randomNumber == timeLeft){
                        gamescoreTextView.text = getString(R.string.score, 100)
                    }

                    if(randomNumber+1 == timeLeft){
                        gamescoreTextView.text = getString(R.string.score, 50)
                    }

                }

            }

        }
    }




}
